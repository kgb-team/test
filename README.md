This is a test, dummy project used by KGB authors to play with web hook
support.

How to enable KGB to your salsa project
=======================================

Add the following URL in Settings→Integrations:

> http://kgb.debian.net:9418/webhook/?channel=``channel-name``

Note that all notifications are copied to ``#commits`` on freenode. If you
don't want that, add ``&private=1`` to the URL.

Enjoy!
